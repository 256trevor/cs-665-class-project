/**
 * Name: Trevor Roach
 * Course: CS-665 Software Designs & Patterns
 * Date: 12/3/2023
 * File Name: Server.java
 * Description: Establishes a Server on a specified port, managing incoming client connections
 *      using a fixed thread pool. Upon client connection, it creates a ClientHandler
 *      to manage client-server interactions, efficiently handling multiple clients concurrently.
 *      The server integrates with GameServer for game state management, ensuring smooth operation
 *      and coordination of the game.
 */
package edu.bu.met.cs665.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static final int PORT = 4545;
    private ServerSocket serverSocket;
    private ExecutorService pool;
    private GameServer gameServer = new GameServer();

    public Server() {
        try {
            System.out.println("Starting the server...");
            serverSocket = new ServerSocket(PORT);
            pool = Executors.newFixedThreadPool(4);
            System.out.println("Server started on port " + PORT);
        } catch (IOException e) {
            System.out.println("Error creating server: " + e.getMessage());
        }
    }

    public void start() {
        System.out.println("Waiting for clients...");
        try {
            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client connected from " + clientSocket.getInetAddress());
                ClientHandler clientHandler = new ClientHandler(clientSocket, gameServer);
                System.out.println("Starting a client handler...");
                pool.execute(clientHandler);
            }
        } catch (IOException e) {
            System.out.println("Server exception: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        Server server = new Server();
        System.out.println("Server is running...");
        server.start();
    }
}
