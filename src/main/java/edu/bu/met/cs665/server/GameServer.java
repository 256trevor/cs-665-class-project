/**
 * Name: Trevor Roach
 * Course: CS-665 Software Designs & Patterns
 * Date: 12/3/2023
 * File Name: GameServer.java
 * Description: GameServer coordinates the game flow and maintains the game state.
 *      It manages player scores, active client connections, and the current number to guess.
 *      The class uses thread-safe collections to maintain state, enabling it to handle concurrent
 *      client interactions. When a player makes a guess, the checkGuess method
 *      is called to determine if the guess is correct, updating the player's score and
 *      broadcasting the result. The server notifies all clients when a player joins or
 *      leaves, ensuring everyone is aware of the game's current participants.
 */
package edu.bu.met.cs665.server;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class GameServer {
    private final ConcurrentHashMap<String, Integer> scores = new ConcurrentHashMap<>();
    private final CopyOnWriteArrayList<ClientHandler> clients = new CopyOnWriteArrayList<>();
    private int numberToGuess;
    private final Random random = new Random();

    public GameServer() {
        setNewNumber();
    }

    public synchronized void checkGuess(int guess, String gamertag) {
        System.out.println(gamertag + " is guessing...");
        if (guess == numberToGuess) {
            int newScore = scores.getOrDefault(gamertag, 0) + 1;
            scores.put(gamertag, newScore);
            broadcastMessage(gamertag + " guessed the correct number and won this round!");
            broadcastScores();
            setNewNumber();
        } else {
            String hint = guess < numberToGuess ? "too low" : "too high";
            broadcastMessage(gamertag + "'s guess of " + guess + " is " + hint + ". Try again!");
        }
    }

    public void addClient(ClientHandler clientHandler) {
        clients.add(clientHandler);
        broadcastMessage(clientHandler.getGamertag() + " has joined the game. Start guessing!");
        System.out.println(clientHandler.getGamertag() + " joined the game. Players can now start guessing.");
        broadcastCurrentPlayers();
    }

    public void removeClient(ClientHandler clientHandler) {
        clients.remove(clientHandler);
        broadcastMessage(clientHandler.getGamertag() + " has left the game.");
        broadcastCurrentPlayers();
    }

    private void setNewNumber() {
        numberToGuess = random.nextInt(50) + 1;
        System.out.println("A new number has been set. Players can start guessing.");
        broadcastMessage("A new round has started. Guess the number between 1 and 50.");
    }

    public void broadcastMessage(String message) {
        for (ClientHandler client : clients) {
            client.sendMessage(message);
        }
    }

    public void broadcastScores() {
        StringBuilder scoreMessage = new StringBuilder("Scores:\n");
        scores.forEach((gamertag, score) -> scoreMessage.append(gamertag).append(": ").append(score).append("\n"));
        broadcastMessage(scoreMessage.toString());
    }

    private void broadcastCurrentPlayers() {
        StringBuilder players = new StringBuilder("Current players: ");
        for (ClientHandler client : clients) {
            players.append(client.getGamertag()).append(", ");
        }
        if (!clients.isEmpty()) {
            players.setLength(players.length() - 2);
        }
        broadcastMessage(players.toString());
        System.out.println(players.toString());
    }
}
