/**
 * Name: Trevor Roach
 * Course: CS-665 Software Designs & Patterns
 * Date: 12/3/2023
 * File Name: ClientHandler.java
 * Description: Manages individual client connections for a multiplayer game server, handling client communication
 *      and game-related interactions. It prompts clients for a gamertag, processes the inputs, and uses
 *      the GameServer to update game states and scores. Also, ensures proper management and
 *      closing of network resources upon client disconnection.
 */
package edu.bu.met.cs665.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientHandler implements Runnable {
    private final Socket socket;
    private PrintWriter send;
    private BufferedReader receive;
    private String gamertag;
    private GameServer gameServer;

    public ClientHandler(Socket socket, GameServer gameServer) {
        this.socket = socket;
        this.gameServer = gameServer;
    }

    @Override
    public void run() {
        try {
            send = new PrintWriter(socket.getOutputStream(), true);
            receive = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            send.println("Enter your gamertag:");
            gamertag = receive.readLine();

            gameServer.broadcastMessage(gamertag + " has joined the game.");
            gameServer.broadcastScores();

            gameServer.addClient(this);

            String inputLine;
            while ((inputLine = receive.readLine()) != null && !socket.isClosed()) {
                if ("exit".equalsIgnoreCase(inputLine)) {
                    send.println("Exiting the game.");
                    break;
                } else if (inputLine.isEmpty()) {
                    send.println("No input received. Please enter a guess.");
                } else {
                    try {
                        int guess = Integer.parseInt(inputLine);
                        gameServer.checkGuess(guess, gamertag);
                    } catch (NumberFormatException e) {
                        send.println("Invalid input. Please enter a valid number.");
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("Client Handler IO Exception: " + e.getMessage());
        } finally {
            closeResources();
            gameServer.broadcastMessage(gamertag + " has left the game.");
            gameServer.broadcastScores();
        }
    }

    private void closeResources() {
        try {
            if (receive != null) receive.close();
            if (send != null) send.close();
            if (socket != null) socket.close();
        } catch (IOException e) {
            System.out.println("Error closing resources: " + e.getMessage());
        }
    }

    public void sendMessage(String message) {
        send.println(message);
    }

    public String getGamertag() {
        return gamertag;
    }
}
