/**
 * Name: Trevor Roach
 * Course: CS-665 Software Designs & Patterns
 * Date: 12/3/2023
 * File Name: Client.java
 * Description: starts the ClientHandler and Game Class
 */
package edu.bu.met.cs665.client;

import java.io.IOException;

public class Client {
    private static final String HOST_IP = "54.156.176.215"; // 127.0.0.1
    private static final int PORT = 4545;

    public static void main(String[] args) {
        try {
            ServerConnecter connection = new ServerConnecter(HOST_IP, PORT);
            Game game = new Game(connection);
            game.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}