/**
 * Name: Trevor Roach
 * Course: CS-665 Software Designs & Patterns
 * Date: 12/3/2023
 * File Name: ServerConnect.java
 * Description: Is responsible for establishing and managing the client's connection to the server.
 *      It attempts to connect to the server using the specified host and port and retries every 10 seconds
 *      if the server is not found. Once a connection is established, it initializes PrintWriter and BufferedReader
 *      objects to handle outgoing and incoming messages, respectively. This class provides methods to send
 *      messages to the server, receive messages from the server, and close the socket connection.
 */
package edu.bu.met.cs665.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.ConnectException;

public class ServerConnecter {
    private Socket socket;
    private PrintWriter send;
    private BufferedReader receive;

    public ServerConnecter(String host, int port) {
        boolean connected = false;
        while (!connected) {
            try {
                socket = new Socket(host, port);
                send = new PrintWriter(socket.getOutputStream(), true);
                receive = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                connected = true;
            } catch (ConnectException e) {
                System.out.println("Server Not Found, retrying in 10 seconds...");
                try {
                    Thread.sleep(10000); //10s
                } catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                    return;
                }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
          }
    }

    public void send(String message) {
        send.println(message);
    }

    public String receive() throws IOException {
        return receive.readLine();
    }

    public void close() throws IOException {
        socket.close();
    }
}