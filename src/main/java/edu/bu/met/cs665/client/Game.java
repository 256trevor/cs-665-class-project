/**
 * Name: Trevor Roach
 * Course: CS-665 Software Designs & Patterns
 * Date: 12/3/2023
 * File Name: Game.java
 * Description: Serves as the client's interface for interacting with the game server.
 *      Upon instantiation, it takes a ServerConnect object to manage communication with the server.
 *      When the game starts, it first waits for a prompt from the server to enter a gamertag, which
 *      the player inputs and sends back. The main game loop then continuously listens for messages from
 *      the server, prompting the player to respond when needed.
 */
package edu.bu.met.cs665.client;

import java.io.IOException;

import java.util.Scanner;

public class Game {
    public static Object Colors;
    private final ServerConnecter client;
    private final Scanner scanner = new Scanner(System.in);

    private static final String RED = "\u001B[33m";
    private static final String GREEN = "\u001B[34m";
    private static final String YELLOW = "\u001B[32m";
    private static final String NOCOLOR = "\u001B[0m";

    public Game(ServerConnecter client) {
        this.client = client;
    }

    public void start() throws IOException {
        String receive = client.receive();
        colorPrint("Server: " + receive, RED);

        colorPrint("Enter your gamertag: ", YELLOW);
        String gamertag = scanner.nextLine();
        client.send(gamertag);

        // Game loop
        while ((receive = client.receive()) != null) {
            colorPrint(":: " + receive , RED);

            if (receive .toLowerCase().contains("guess") ||
                    receive .toLowerCase().contains("set the number") ||
                    receive .toLowerCase().contains("setting the next number")) {
                colorPrint("Enter your response: ", YELLOW);
                String fromUser = scanner.nextLine();
                client.send(fromUser);
            }
        }
    }

    private void colorPrint(String text, String colorCode) {
        System.out.println(colorCode + text + NOCOLOR);
    }
}
