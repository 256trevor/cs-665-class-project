package edu.bu.met.cs665;

import edu.bu.met.cs665.client.Game;
import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class GameTester {

    private Game game;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Test
    public void testConstructorInitialization() {
        assertNotNull("Game object should not be null", game);

    }


    @Test(expected = IOException.class)
    public void testExceptionHandling() {

    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }
}
