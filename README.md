
| CS-665       | Software Design & Patterns |
|--------------|----------------------------|
| Name         | Trevor Roach               |
| Date         | 12/4/23                    |
| Course       | Fall                       |
| Assignment # | Class Project              |

# Assignment Overview
Demonstrate your software design skills by solving a problem that interests you 
and presents a suitable challenge.
Do not only use already covered patterns 
Strategy, Factory Method, Abstract Factory, Singleton, 
Prototype, Command, Observer, State, Template, Facade,
Decorator, Composite, Adapter, Proxy, Iterator, and Mediator patterns. 

Main objective for the class project is to expand your knowledge of design patterns. You
should choose a design pattern that was not covered in our class, study it on your own, develop
a unique use case scenario for it, and create a presentation that showcases the design pattern and your scenario. 
Find one listed on the website Java Design Patterns. https://java-design-patterns.com/patterns/
Some examples of important patterns that were not covered in depth in our class
include the Bridge, Builder, Callback, Delegation, and Thread Pool patterns.

#### Implementation Description in a README.md (2 points)
- Explain the level of flexibility in your implementation, including how new object types can
be easily added or removed in the future.
- Discuss the simplicity and understandability of your implementation, ensuring that it is
easy for others to read and maintain.
- Describe how you have avoided duplicated code and why it is important.
- If applicable, mention any design patterns you have used and explain why they were
chosen.


# GitHub Repository Link:
https://gitlab.com/256trevor/cs-665-class-project.git

# Implementation Description
The Game runs by starting client.java and ether connecting to the server locally run with Server.java, or hosted on Port 4545.

Server.java file uses the Thread Pool design pattern. 
This pattern is typically identified by the use of an ExecutorService or execute method, which is a common way to manage a pool of threads in Java. 
This is evidenced by the use of ExecutorService and the method call to Executors.newFixedThreadPool(4), which creates a 
thread pool with a fixed number of threads.

In the start method, for every incoming client connection accepted by the ServerSocket, a new ClientHandler is created, 
and instead of starting a new thread for each ClientHandler, the execute method of the ExecutorService (represented by the 
variable pool) is called. This method submits the ClientHandler to be run by one of the threads managed by the thread pool. 
This approach is more resource-efficient than creating a new thread for every client because it reuses a fixed number of
threads to handle multiple clients, which can be particularly beneficial under high load, as it limits the number of
concurrent threads and helps to manage the resources more effectively.
![Screenshot_2023-12-05_11-38-29.png](Screenshot_2023-12-05_11-38-29.png)
### Thread Pool considerations
The AWS t4g.nano instance type, has a 2 vCPUs. The maximum number of threads that can be efficiently created the nature of the tasks the threads are executing (CPU-bound vs. I/O-bound), the server's CPU power, and its memory limitations.

For CPU-bound tasks, the amount of threads closes to the number of vCPUs. Having more threads than CPU cores generally doesn't result in better performance, causes overhead.

For I/O-bound (disk reads, writes, or network), you can have more threads than the number of vCPUs. This is because I/O-bound tasks spend a significant amount of time waiting for I/O operations to complete, during which time other threads can use the CPU.

The "maximum" number of threads is not just about what the CPU can handle, but also about the overall resource usage and application design.



![UML.png](UML.png)





# Maven Commands
## Compile
```bash
mvn clean compile
```

## JUnit Tests
```bash
mvn clean test
```

## Spotbugs
```bash
mvn spotbugs:gui 
```

## Checkstyle
```bash
mvn checkstyle:checkstyle
```